# Crossplane
IaaS Tool using K8S approach.
More can be found here: https://crossplane.io

Crossplane related stuff can be found here

## Install Crossplane (via Helm)
```
# create deditaced namespace
kubectl create ns crossplane-system

# add crossplane helm repo
helm repo add crossplane-stable https://charts.crossplane.io/stable 
helm repo update

# install crossplane release
helm install crossplane -n crossplane-system crossplane-stable/crossplane

# check deployment of crossplane's objects
kubectl get all -n crossplane-system

# install crossplane CLI
curl -sL https://raw.githubusercontent.com/crossplane/crossplane/master/install.sh | sh
sudo mv kubectl-crossplane /usr/bin
kubectl crossplane --help
```

## Install Crossplane Provider (AWS)
```
# install provider via crossplane 
kubectl crossplane install provider crossplane/provider-aws:v0.27.0
kubectl get provider

# check deployed objects
kubectl get deploy -n crossplane-system
kubectl get crd -n crossplane-system

```

## Provider config
```
# create config file with AWS credentials
cat creds.conf
[default]
aws_access_key_id =  xxxxxxxxxxxxx
aws_secret_access_key = yyyyyyyyyyyyyyyyyyy

# create secret from given file
kubectl create secret generic aws-creds -n crossplane-system --from-file=creds=/root/creds.conf
```

## Apply configs
``` 
# apply authentication method
kubectl create -f providerConfig.yaml

# check 
kubectl get ProviderConfig
```

## Deploy AWS resources
```
cd ec2/
kubectl create -f vpc.yaml
```
